<?php

namespace Drupal\Tests\funnelback\Unit;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\funnelback\Funnelback;
use Drupal\funnelback\FunnelbackClient;
use Drupal\funnelback\FunnelbackQueryString;
use Drupal\Tests\UnitTestCase;
use Drupal\Component\Serialization\Json;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;

/**
 * The FunnelbackTestCase test.
 *
 * @group funnelback
 */
class FunnelbackTest extends UnitTestCase {

  protected $successResponse;

  protected $suggestResponse;

  protected $failResponse;

  protected $emptyResponse;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $requestStack = $this->getMockBuilder(RequestStack::class)
      ->disableOriginalConstructor()
      ->getMock();

    $configFactory = $this->getMockBuilder(ConfigFactoryInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $configFactory->expects($this->any())
      ->method('get')
      ->willReturn(NULL);

    $moduleHandler = $this->getMockBuilder(ModuleHandlerInterface::class)
      ->disableOriginalConstructor()
      ->getMock();

    $funnelbackClient = $this->getMockBuilder(FunnelbackClient::class)
      ->disableOriginalConstructor()
      ->getMock();

    $funnelbackQuerystring = new FunnelbackQueryString();

    $funnelback = new Funnelback($requestStack, $configFactory, $moduleHandler, $funnelbackQuerystring, $funnelbackClient);

    $container = new ContainerBuilder();
    $container->set('funnelback.funnelback', $funnelback);
    $container->set('funnelback.querystring', $funnelbackQuerystring);
    \Drupal::setContainer($container);

    $this->successResponse = new Response(
      file_get_contents(dirname(__FILE__) . '/../../mocks/response.json', FILE_USE_INCLUDE_PATH),
      200,
      []
    );

    $this->failResponse = new Response(
      '',
      404,
      []
    );

    $this->emptyResponse = new Response(
      file_get_contents(dirname(__FILE__) . '/../../mocks/failresponse.json', FILE_USE_INCLUDE_PATH),
      200,
      []
    );

    $this->suggestResponse = new Response(
      file_get_contents(dirname(__FILE__) . '/../../mocks/suggestresponse.json', FILE_USE_INCLUDE_PATH),
      200,
      []
    );

  }

  /**
   * Test funnelback_json_query function.
   */
  public function testDoQuery() {
    $response = \Drupal::service('funnelback.funnelback')->funnelbackJsonQuery(Json::decode($this->successResponse->getContent()), '');

    $this->assertEquals($this->getJsonQueryResult(), $response['results']);

    $response = \Drupal::service('funnelback.funnelback')->funnelbackJsonQuery(Json::decode($this->failResponse->getContent()), '');

    $this->assertEquals(NULL, $response);

    $response = \Drupal::service('funnelback.funnelback')->funnelbackJsonQuery(Json::decode($this->emptyResponse->getContent()), '');

    $this->assertEquals([], $response);
  }

  /**
   * Test autocomplete response.
   */
  public function testSuggestQuery() {
    $response = \Drupal::service('funnelback.funnelback')->funnelbackJsonQuery(Json::decode($this->suggestResponse->getContent()), '');
    $this->assertEquals($this->getSuggestJsonQueryResult(), $response);
  }

  /**
   * Test filter facet query string function.
   */
  public function testFilterFacetQueryString() {
    $raw_strings = [
      'query=holden',
      'f.Condition%7Ccondition=<span>demo</span>',
      'f.Extras%7Extras=aircondition',
      'f.Extras%7Extras=<span>cruise+control</span>',
    ];

    $facet_strings = [
      'f.Condition%7Ccondition' => [
        'demo',
      ],
      'f.Extras%7Extras' => [
        'aircondition',
        'cruise+control',
      ],
    ];

    $this->assertEquals($facet_strings, \Drupal::service('funnelback.querystring')->funnelbackFilterFacetQueryString($raw_strings));
  }

  /**
   * Test filter system query string function.
   */
  public function testFilterSystemQueryString() {
    $testQueryString = '?remote_ip=10.20.1.1&query=holden&profile=_default_preview&f.Condition%7CAllDocumentsFill=All&collection=fb-bargain-store-meta&form=custom_json';
    $fiteredQueryString = '?query=holden&f.Condition%7CAllDocumentsFill=All';

    $this->assertEquals($fiteredQueryString, \Drupal::service('funnelback.querystring')->filterQueryString($testQueryString));
  }

  /**
   * Test filter contextual query string function.
   */
  public function testContextualFilter() {
    $raw_strings = [
      'cluster1=my+holden',
      'cluster0=holden',
      'start_rank=0',
      'query=About+my+holden',
      'cluster2=<span>about+holden</span>',
    ];

    $contextual_strings = [
      'cluster1' => 'my+holden',
      'cluster0' => 'holden',
      'cluster2' => 'about+holden',
    ];

    $this->assertEquals($contextual_strings, \Drupal::service('funnelback.querystring')->funnelbackFilterContextualQueryString($raw_strings));
  }

  /**
   * Test query normaliser.
   */
  public function testQueryNormaliser() {
    $raw_string = "collection=test&query=`my holden is awesome`&f_Condition%7Ccondition[0]=<script>demo</script>";
    $expected_string = "collection=test&query=my+holden+is+awesome&f.Condition|condition=demo";

    $this->assertEquals($expected_string, \Drupal::service('funnelback.querystring')->funnelbackQueryNormaliser($raw_string));
  }

  /**
   * Test qurator link filter.
   */
  public function testQuratorLink() {
    $raw_string = '/s/redirect?collection=cars&url=http%3A%2F%2Fgoogle.com&auth=21XsizHQ7f0vU3r3%2Fmw%2BAg&profile=_default_preview&type=FP';
    $expected_string = 'http://google.com';

    $this->assertEquals($expected_string, \Drupal::service('funnelback.querystring')->filterCuratorLink($raw_string));
  }

  /**
   * Test string remover from raw query strings.
   */
  public function testStringRemover() {
    $queryString = [
      'f.Profile|profile=cars',
      'query=holden',
      'f.Content+type|contentType=node',
      'start_rank=1',
      'start_rank=11',
    ];
    $expected_string = [
      'f.Profile|profile=cars',
      'query=holden',
      'f.Content+type|contentType=node',
    ];
    $this->assertEquals($expected_string, \Drupal::service('funnelback.querystring')->funnelbackQueryRemove('start_rank', $queryString));
  }

  /**
   * Returns JSON result.
   */
  protected function getJsonQueryResult() {
    return [
      [
        'title' => 'https://sample.data.io/Holden/Demo/Blue/2',
        'date' => '1485781200000',
        'summary' => NULL,
        'live_url' => 'sample.data.io/Holden/Demo/Blue/2',
        'cache_url' => '/s/cache?collection=fb-cars-xml&doc=funnelback-web-crawl.warc&off=408&len=353&url=https%3A%2F%2Fsample.data.io%2FHolden%2FDemo%2FBlue%2F2&profile=_default_preview',
        'display_url' => 'https://sample.data.io/Holden/Demo/Blue/2',
        'metaData' => [
          'condition' => 'Demo',
          'colour' => 'Blue',
          'd' => '31 January 2017',
          'price' => '25000',
          'extras' => 'Aircondition, Power Steering, Electric Windows, Cruise Control',
          'make' => 'Holden',
          'nodeId' => 1,
        ],
      ],
    ];
  }

  /**
   * Returns JSON suggestion.
   */
  protected function getSuggestJsonQueryResult() {
    return [
      "hdmi" => 'hdmi',
      'hp' => 'hp',
      'holden' => 'holden',
    ];
  }

}
