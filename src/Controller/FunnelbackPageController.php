<?php

namespace Drupal\funnelback\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Lock\NullLockBackend;
use Drupal\Core\Messenger\Messenger;
use Drupal\funnelback\Funnelback;
use Drupal\funnelback\FunnelbackQueryString;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Pager\PagerManagerInterface;

/**
 * The FunnelbackPageController controller.
 */
class FunnelbackPageController extends ControllerBase {

  /**
   * Entity manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityManager;

  /**
   * @var \Drupal\funnelback\Funnelback
   */
  protected $funnelback;

  /**
   * @var \Drupal\funnelback\FunnelbackQueryString
   */
  protected $funnelbackQueryString;

  /**
   * @var \Drupal\Core\Messenger\Messenger
   */
  protected $messenger;

  /**
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * @var \Drupal\Core\Pager\PagerManagerInterface
   */
  protected $pagerManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entity_manager, Funnelback $funnelback, FunnelbackQueryString $funnelbackQueryString, Messenger $messenger, ConfigFactoryInterface $configFactory, PagerManagerInterface $pager_manager) {
    $this->entityManager = $entity_manager;
    $this->funnelback = $funnelback;
    $this->funnelbackQueryString = $funnelbackQueryString;
    $this->messenger = $messenger;
    $this->configFactory = $configFactory;
    $this->pagerManager = $pager_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('funnelback.funnelback'),
      $container->get('funnelback.querystring'),
      $container->get('messenger'),
      $container->get('config.factory'),
      $container->get('pager.manager')
    );
  }

  /**
   * Page callback for funnelback search.
   */
  public function getSearchView() {

    // Create an array of query params.
    $request_params = \Drupal::request()->query->all();

    if (!isset($request_params['query'])) {
      return [
        '#theme' => 'funnelback_results',
        '#items' => [],
        '#total' => 0,
        '#query' => NULL,
        '#curator' => [],
        '#pager' => [],
        '#summary' => NULL,
        '#breadcrumb' => [],
        '#spell' => [],
        '#no_result_text' => NULL,
        '#attached' => [
          'library' => [
            'funnelback/funnelback',
          ],
        ],
      ];
    }
    else {
      $query = Xss::filter($request_params['query']);
    }

    // Find the start rank form pager query string.
    $pageSize = $this->configFactory->get('funnelback.settings')->get('general_settings.pagesize');
    $start = (!empty($request_params['page'])) ? Xss::filter($request_params['page'] * $pageSize + 1) : 0;

    $raw_queries = explode('&', $_SERVER['QUERY_STRING']);

    // Filter the facet params out.
    $facet_query = $this->funnelbackQueryString->funnelbackFilterFacetQueryString($raw_queries);

    // Filter the contextual params out.
    $contextual_query = $this->funnelbackQueryString->funnelbackFilterContextualQueryString($raw_queries);

    $results = $this->funnelback->funnelbackDoQuery($query, $start, NULL, $facet_query, $contextual_query, funnelback_get_config()->get('general_settings.custom_template'));

    $output = [];

    // Sanitise no-result-text with query token.
    $no_result_text = $this->configFactory->get('funnelback.settings')->get('general_settings.no_result_text');
    $no_result_text = Xss::filterAdmin(str_replace('[funnelback-query]', $query, $no_result_text));

    if (!empty($results) && $this->funnelback->funnelbackResultValidator($results)) {

      // Composer array of rendered results.
      $items = [];
      foreach ($results['results'] as $result) {
        // Use funnelback summary by default.
        $render_item = [
          '#theme' => 'funnelback_result',
          '#display_url' => $result['display_url'],
          '#live_url' => $result['live_url'],
          '#title' => $result['title'],
          '#date' => $result['date'],
          '#summary' => $result['summary'],
          // Pass any custom metadata to theme for further customisation.
          '#metadata' => $result['metaData'],
        ];
        $item = $render_item;

        // Use view mode if local node.
        if ($result['metaData']['nodeId'] && $this->configFactory->get('funnelback.settings')->get('display_mode.enabled')) {
          // NodeId and view mode are available. Use view mode to render node.
          $view_mode = $this->configFactory->get('funnelback.settings')->get('display_mode.id');
          $node = $this->entityManager->getStorage('node')->load($result['metaData']['nodeId']);
          // Check if node exists. If not just use default Funnelback result.
          if ($node) {
            $item = $this->entityManager
              ->getViewBuilder('node')
              ->view($node, $view_mode);
          }
        }

        $items[] = $item;
      }

      $summary = [
        '#theme' => 'funnelback_summary',
        '#summary' => $results['summary'],
      ];

      // Detect if there is any filter selected.
      $selected = FALSE;
      foreach ($results['facets'] as $facet) {
        if ($facet['selected'] == TRUE) {
          $selected = TRUE;
        }
      }
      $this->funnelback->funnelbackFilterFacetDisplay($results['facets']);
      $breadcrumb = [
        '#theme' => 'funnelback_breadcrumb',
        '#facets' => $results['facets'],
        '#facet_extras' => $results['facetExtras'],
        '#selected' => $selected,
      ];
      $spell = [
        '#theme' => 'funnelback_spell',
        '#spell' => $results['spell'],
      ];
      $curator = [
        '#theme' => 'funnelback_curator',
        '#curator' => $results['curator'],
      ];

      // Now that we have the total number of results, initialize the pager.
      $this->pagerManager->createPager($results['summary']['total'], $pageSize);
      $pager = [
        '#type' => 'pager',
        '#summary' => $results['summary'],
      ];


      $output = [
        '#theme' => 'funnelback_results',
        '#items' => $items,
        '#total' => $results['summary']['total'],
        '#query' => $results['summary']['query'],
        '#curator' => $curator,
        '#pager' => $pager,
        '#summary' => $summary,
        '#breadcrumb' => $breadcrumb,
        '#spell' => $spell,
        '#no_result_text' => $no_result_text,
        '#attached' => [
          'library' => [
            'funnelback/funnelback',
          ],
        ],
      ];
    }
    else {
      $output = [
        '#theme' => 'funnelback_results',
        '#items' => [],
        '#total' => 0,
        '#query' => $results['summary']['query'],
        '#curator' => NULL,
        '#pager' => NULL,
        '#summary' => NULL,
        '#breadcrumb' => NULL,
        '#spell' => NULL,
        '#no_result_text' => $no_result_text,
        '#attached' => [
          'library' => [
            'funnelback/funnelback',
          ],
        ],
      ];
    }

    return $output;
  }

  /**
   * Page callback for autocompletion request.
   *
   * @param Request $request
   *   Request to autocomplete URL.
   *
   * @return string $suggest
   */
  public function getAutocompletionResult(Request $request) {

    $partial_query = $request->query->get('q');
    $suggests = [];
    if ($partial_query) {
      $partial_query = Xss::filter($partial_query);
      $results = $this->funnelback->funnelbackDoQuery(NULL, NULL, $partial_query, NULL, NULL, NULL);

      // Add key to suggest array.
      $suggests = [];
      foreach ($results as $result) {
        $suggests[] = [
          'label' => $result['key'],
          'value' => $result['key'],
        ];
      }
    }

    return new JsonResponse($suggests);
  }

}
