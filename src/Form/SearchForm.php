<?php

namespace Drupal\funnelback\Form;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Funnelback search form.
 */
class SearchForm extends FormBase {

  /**
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * SearchForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   */
  public function __construct(ConfigFactoryInterface $configFactory) {
    $this->configFactory = $configFactory;
  }

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *
   * @return \Drupal\Core\Form\FormBase|\Drupal\funnelback\Form\SearchForm
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'funnelback_search';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $formState) {
    // Find out current search query.
    $query_string = \Drupal::request()->query->all();
    $query = '';

    if (isset($query_string['query'])) {
      $query = Xss::filter($query_string['query']);
      $query = str_replace("`", '', $query);
    }

    $form['funnelback_search_field'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Search'),
      '#title_display' => 'invisible',
      '#size' => 15,
      '#default_value' => $query,
      '#attributes' => ['title' => $this->t('Enter the terms you wish to search for.')],
    ];

    // Add auto-completion.
    $auto_completion = $this->configFactory->get('funnelback.settings')->get('autocomplete.enabled');
    if ($auto_completion) {
      $form['funnelback_search_field']['#autocomplete_route_name'] = 'funnelback.search.autocompletion';
    }

    $form['funnelback_search_submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Search'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $formState) {
    $query = Xss::filter($formState->getvalue('funnelback_search_field'));

    $url = Url::fromRoute('funnelback.search');
    $url->setOption('query', ['query' => $query]);

    $formState->setRedirectUrl($url);

    return;
  }

}
