<?php

namespace Drupal\funnelback\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\funnelback\Funnelback;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides a block for funnelback facets.
 *
 * @Block(
 *   id = "funnelback_facet_block",
 *   admin_label = @Translation("Funnelback facets"),
 *   category = @Translation("Funnelback")
 * )
 */
class FacetBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\funnelback\Funnelback
   */
  protected $funnelback;

  /**
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $request;

  /**
   * ContextualNavigationBlock constructor.
   *
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param \Drupal\funnelback\Funnelback $funnelback
   * @param RequestStack $request
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Funnelback $funnelback, RequestStack $request) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->funnelback = $funnelback;
    $this->request = $request;
  }

  /**
   * @param ContainerInterface $container
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   *
   * @return \Drupal\Core\Plugin\ContainerFactoryPluginInterface|\Drupal\funnelback\Plugin\Block\ContextualNavigationBlock
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('funnelback.funnelback'),
      $container->get('request_stack')
    );
  }
  /**
   * {@inheritdoc}
   */
  public function build() {
    $results = $this->funnelback->funnelbackStaticResultsCache();
    $output = [];
    if ($this->funnelback->funnelbackResultValidator($results)) {
      $facets = $results['facets'];
      $query = $results['summary']['query'];

      // Only support single dropdown, checkbox and radio button at the moment.
      $this->funnelback->funnelbackFilterFacetDisplay($facets);

      $output = [
        '#theme' => 'funnelback_facets_block',
        '#facets' => $facets,
        '#query' => $query,
        "#attached" => [
          'library' => [
            'funnelback/funnelback.facet',
          ],
        ],
      ];
    }

    return $output;
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'use funnelback search');
  }

  /**
   * {@inheritDoc}
   */
  public function getCacheTags() {
    return Cache::mergeTags(parent::getCacheTags(), [
      'funnelback:' . $this->request->getCurrentRequest()->getQueryString(),
    ]);
  }

  /**
   * {@inheritDoc}
   */
  public function getCacheContexts() {
    return Cache::mergeContexts(parent::getCacheContexts(), ['url.query_args']);
  }

}
