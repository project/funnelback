<?php

namespace Drupal\funnelback;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Logger\LoggerChannelFactory;
USE Drupal\Core\Logger\RfcLogLevel;
use Drupal\Core\Url;
use GuzzleHttp\Exception\RequestException;

/**
 * Service class for funnelback client.
 */
class FunnelbackClient {

  protected $debugNone = 'none';
  protected $debugLog = 'log';
  protected $debugVerbose = 'verbose';

  /**
   * @var \Drupal\funnelback\FunnelbackQueryString
   */
  protected $funnelbackQueryString;

  /**
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $logger;

  /**
   * FunnelbackClient constructor.
   *
   * @param \Drupal\funnelback\FunnelbackQueryString $funnelbackQueryString
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   * @param \Drupal\Core\Logger\LoggerChannelFactory $logger
   */
  public function __construct(FunnelbackQueryString $funnelbackQueryString, ConfigFactoryInterface $configFactory, LoggerChannelFactory $logger) {
    $this->funnelbackQueryString = $funnelbackQueryString;
    $this->configFactory = $configFactory;
    $this->logger = $logger;
  }

  /**
   * Make a request.
   *
   * @param string $baseUrl
   *   The base URL for the request.
   * @param string $apiPath
   *   The api path from the base URL.
   * @param array $requestParams
   *   The request parameters.
   *
   * @return object
   *   The response object.
   *
   * @throws
   */
  public function request($baseUrl, $apiPath, array $requestParams) {

    $response = NULL;

    if ($baseUrl !== '/') {
      // Build the search URL with query params.
      $url = $baseUrl . $apiPath . '?' . http_build_query($requestParams);

      $url = $this->funnelbackQueryString->funnelbackQueryNormaliser($url);

      // Make the request.
      try {
        $response = \Drupal::httpClient()->request('get', $url);

        $this->debug('Requesting url: %url. Response %response. Template %template', [
          '%url' => $url,
          '%response' => $response->getStatusCode(),
          '%template' => ($apiPath == 's/search.json') ? t('Default') : t('Custom'),
        ]);
      }
      catch (RequestException $e) {
        $this->debug('Fail to connect to Funnelback server.');
      }
    }

    return $response;
  }

  /**
   * Helper to log debug messages.
   *
   * @param string $message
   *   A message, suitable for watchdog().
   * @param array $args
   *   (optional) An array of arguments, as per watchdog().
   * @param int $logLevel
   *   (optional) The watchdog() log level. Defaults to WATCHDOG_DEBUG.
   */
  public function debug($message, array $args = [], $logLevel = 7) {

    $config = $this->configFactory->get('funnelback.settings');
    $debug = $config->get('general_settings.debug_mode');
    $string = new FormattableMarkup($message, $args);
    if ($debug == $this->debugLog) {

      $this->logger->get('funnelback')->notice($string);
    }
    elseif ($debug == $this->debugVerbose) {
      if ($logLevel >= RfcLogLevel::ERROR) {
        $messageLevel = 'error';
      }
      else {
        $messageLevel = 'status';
      }
      \Drupal::messenger()->addMessage($string, $messageLevel);
    }
  }

}
